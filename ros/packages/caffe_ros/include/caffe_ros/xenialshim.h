#ifndef CAFFE_ROS_XENIALSHIM_H
#define CAFFE_ROS_XENIALSHIM_H

// source: https://github.com/NVIDIA/TensorRT/blob/v6.0.1/include/NvInfer.h

namespace nvinfer1
{

enum class DimensionType : int
{
    kSPATIAL = 0, //!< Elements correspond to different spatial data.
    kCHANNEL = 1, //!< Elements correspond to different channels.
    kINDEX = 2,   //!< Elements correspond to different batch index.
    kSEQUENCE = 3 //!< Elements correspond to different sequence values.
};

class DimsCHW : public Dims3
{
public:
    DimensionType type[MAX_DIMS];
    //!
    //! \brief Construct an empty DimsCHW object.
    //!
    DimsCHW()
        : Dims3()
    {
        type[0] = DimensionType::kCHANNEL;
        type[1] = type[2] = DimensionType::kSPATIAL;
    }

    //!
    //! \brief Construct a DimsCHW given channel count, height and width.
    //!
    //! \param channels The channel count.
    //! \param height The height of the data.
    //! \param width The width of the data.
    //!
    DimsCHW(int channels, int height, int width)
        : Dims3(channels, height, width)
    {
        type[0] = DimensionType::kCHANNEL;
        type[1] = type[2] = DimensionType::kSPATIAL;
    }

    //!
    //! \brief Get the channel count.
    //!
    //! \return The channel count.
    //!
    int& c() { return d[0]; }

    //!
    //! \brief Get the channel count.
    //!
    //! \return The channel count.
    //!
    int c() const { return d[0]; }

    //!
    //! \brief Get the height.
    //!
    //! \return The height.
    //!
    int& h() { return d[1]; }

    //!
    //! \brief Get the height.
    //!
    //! \return The height.
    //!
    int h() const { return d[1]; }

    //!
    //! \brief Get the width.
    //!
    //! \return The width.
    //!
    int& w() { return d[2]; }

    //!
    //! \brief Get the width.
    //!
    //! \return The width.
    //!
    int w() const { return d[2]; }
};

}

#endif
